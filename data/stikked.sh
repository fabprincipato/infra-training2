declare -ag data_stikked_packages

data_stikked_packages=( git )
data_stikked_url="https://github.com/claudehohl/Stikked"
data_stikked_tag="0.10.0"
data_stikked_path="/var/www/html/stikked"
data_stikked_captcha_private="6Lc5IxwTAAAAADUbKKIe2g5kq7kUOtXNkXFFyear"
data_stikked_captcha_public="6Lc5IxwTAAAAAMr-oQUW7eXU6e5gGTXmLTRcbLpJ"
