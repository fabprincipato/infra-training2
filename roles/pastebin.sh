#https://github.com/claudehohl/Stikked
stdlib.data mysql-common
stdlib.data mysql-pastebin
stdlib.data apache-common
stdlib.data apache-php
stdlib.data stikked 

stdlib.enable_mysql
stdlib.enable_apache
stdlib.enable_augeas

stdlib.profile mysql/packages
stdlib.profile mysql/provisioning
stdlib.profile apache/packages
stdlib.profile apache/php
stdlib.profile stikked/install
stdlib.profile stikked/configure

