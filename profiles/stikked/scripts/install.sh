stdlib.title "stikked/install"

for package in ${data_stikked_packages[@]}
do
    stdlib.apt --package $package
done

stdlib.git --name $data_stikked_path --source $data_stikked_url --tag $data_stikked_tag

