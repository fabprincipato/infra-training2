stdlib.title "stikked/configure"

stdlib.file --name $data_stikked_path/htdocs/application/config/stikked.php --source "$profile_path/files/stikked.php" --mode 0644
stdlib.file_line  --file $data_stikked_path/htdocs/application/config/stikked.php \
		  --line "\$config[\"recaptcha_publickey\"] = \"$data_stikked_captcha_public\";" --match "\$config\[\x27recaptcha_publickey\x27\]"

stdlib.file_line  --file $data_stikked_path/htdocs/application/config/stikked.php \
		  --line "\$config[\"recaptcha_privatekey\"] = \"$data_stikked_captcha_private\";" --match "\$config\[\x27recaptcha_privatekey\x27\]"

# change document root
apache.setting --path "VirtualHost=*:80" \
               --key DocumentRoot --value $data_stikked_path/htdocs \
               --file /etc/apache2/sites-enabled/000-default.conf

apache.section --path "VirtualHost=*:80" --type Directory --name $data_stikked_path/htdocs \
               --file /etc/apache2/sites-enabled/000-default.conf

apache.setting --path "VirtualHost=*:80" \
	       --path "Directory=$data_stikked_path/htdocs" \
               --key Require --value "all granted" \
               --file /etc/apache2/sites-enabled/000-default.conf

apache.setting --path "VirtualHost=*:80" \
	       --path "Directory=$data_stikked_path/htdocs" \
               --key Options --value "Indexes FollowSymLinks" \
               --file /etc/apache2/sites-enabled/000-default.conf

apache.setting --path "VirtualHost=*:80" \
	       --path "Directory=$data_stikked_path/htdocs" \
               --key AllowOverride --value "All" \
               --file /etc/apache2/sites-enabled/000-default.conf

apache.setting --path "VirtualHost=*:80" \
	       --path "Directory=$data_stikked_path/htdocs" \
               --key Order --value "allow,deny" \
               --file /etc/apache2/sites-enabled/000-default.conf

apache.setting --path "VirtualHost=*:80" \
	       --path "Directory=$data_stikked_path/htdocs" \
               --key Allow --value "from all" \
               --file /etc/apache2/sites-enabled/000-default.conf
stdlib_state_change=true
if [[ $stdlib_state_change == true ]]; then
 stdlib.capture_error /etc/init.d/apache2 restart
fi

