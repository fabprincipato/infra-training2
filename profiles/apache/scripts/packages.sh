stdlib.title "apache/packages"

for package in ${data_apache_packages[@]}
do
    stdlib.apt --package $package
done

stdlib.sysvinit --name apache2

stdlib.capture_error update-rc.d apache2 defaults 

apache.setting --path "IfModule=mod_info.c" \
	       --path "Location=/server-info" \
     	       --key Require --value "ip 83.244.213.90/32" \
	       --file /etc/apache2/mods-available/info.conf					

apache.setting --path "IfModule=mod_status.c" \
	       --path "Location=/server-status" \
     	       --key Require --value "ip 83.244.213.90/32" \
	       --file /etc/apache2/mods-available/status.conf					

if [ ! -f /etc/apache2/mods-enabled/info.load ]; then
  stdlib.capture_error a2enmod info
  stdlib_state_change=true
fi

if [ ! -f /etc/apache2/mods-enabled/status.load ]; then
  stdlib.capture_error a2enmod status
  stdlib_state_change=true
fi

if [ ! -f /etc/apache2/mods-enabled/rewrite.load ]; then
  stdlib.capture_error a2enmod rewrite
  stdlib_state_change=true
fi

if [[ $stdlib_state_change == true ]]; then
 stdlib.capture_error /etc/init.d/apache2 restart
fi
